k = int(input())
k = k // 4

def is_ryf(s1, s2):
	if len(s1[-1]) < 2 or len(s2[-1]) < 2:
		return False

	if (s1[-1][-2:] == s2[-1][-2:]):
		return True
	return False

t1 = True
t2 = True
t3 = True

for i in range(0, k):
	s1 = input().split()
	s2 = input().split()
	s3 = input().split()
	s4 = input().split()

	if not(is_ryf(s1, s2) and is_ryf(s3, s4) and t1):
		t1 = False
	if not(is_ryf(s1, s3) and is_ryf(s2, s4) and t2):
		t2 = False
	if not(is_ryf(s1, s4) and is_ryf(s2, s3) and t3):
		t3 = False

if t1 or t2 or t3:
	print("Yes")
else:
	print("No")
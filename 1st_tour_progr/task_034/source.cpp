#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    long long sum = 0;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
        sum += a[i];
    }
    if (sum % n != 0) {
        cout << -1 << '\n';
        return 0;
    }
    long long k = sum / n;
    long long ans = 0;
    int j = 0;
    for (int i = 0; i < n; i++) {
        if (a[i] >= k) {
            continue;
        }
        while (a[i] != k) {
            while (j < n && a[j] <= k) {
                j++;
            }
            int t = min(k - a[i], a[j] - k);
            a[j] -= t;
            a[i] += t;
            ans += t * abs(i - j);
        }
    }
    cout << ans << '\n';
}
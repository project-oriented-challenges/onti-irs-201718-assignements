def generate():
    return []

def check(reply, clue):
    return reply.strip() == clue.strip()

x = []
y = []

# triangle area
def area(a,b,c):
    s = (x[b] - x[a]) * (y[c] - y[a]) - (y[b] - y[a]) * (x[c] - x[a])
    return -1 if s < 0 else (1 if s > 0 else 0)

# bounding box
def box(a, b, c, d):
    return max(min(a, b), min(c, d)) <= min(max(a, b), max(c, d))

# пересечение отрезков
def intersect(a, b, c, d):
    return box(x[a],x[b],x[c],x[d]) & 
           box(y[a],y[b],y[c],y[d]) & 
           (area(a, b, c) * area(a, b, d) <= 0) & 
           (area(c, d, a) * area(c, d, b) <= 0)

def solve(dataset):
    ds = dataset.splitlines()

    s = ds[0].split()
    n = int(s[0])
    m = int(s[1])

    s = ds[1].split()
    for i in range(n):
        x.append(int(s[i*2]))
        y.append(int(s[i*2+1]))

    s = ds[2].split()
    for i in range(m):
        x.append(int(s[i * 2]))
        y.append(int(s[i * 2 + 1]))

    inter = False

    for i in range(n - 1):
        for j in range(m - 1):
            inter |= intersect(i, i + 1, n + j, n + j + 1)

    return "Yes" if inter else "No"